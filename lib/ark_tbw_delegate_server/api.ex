defmodule ArkTbwDelegateServer.API do
  alias ArkElixir.Account, as: AAccount
  alias ArkElixir.Client, as: AC
  alias ArkElixir.Util.TransactionBuilder, as: ATB

  alias PersonaElixir.Account, as: PAccount
  alias PersonaElixir.Client, as: PC
  alias PersonaElixir.Util.TransactionBuilder, as: PTB

  @devnet_nethash "578e820911f24e039733b45e4882b73e301f813a0d2c31330dafda8" <>
    "4534ffa23"

  @devnet_nethash_persona "b4e87739ca85f7eabf844a643930573e9a2dd9da291662e74" <>
    "d26962b5c3f0ed9"

  @mainnet_nethash "6e84d08bd299ed97c212c886c98a57e36545c8f5d645ca7eeae63a8b" <>
    "d62d8988"

  @mainnet_nethash_persona "14b55c1de06caa015362d59ad97a144bc3c9fc2b50ece84b" <>
    "78d13ceaeaf7d8fb"

  def blocks(client, params) do
    # NOTE: For now the code is the same between the two libs
    ArkElixir.Block.blocks(client, params)
  end

  def client_from_peer(peer, %{delegate_address: "A" <> _remainer} = opts) do
    AC.new(%{
      ip: peer.ip,
      nethash: opts.nethash,
      network_address: opts.network_address,
      port: peer.port,
      protocol: "http", # :(
      version: peer.version
    })
  end

  def client_from_peer(peer, %{delegate_address: "D" <> _remainer} = opts) do
    AC.new(%{
      ip: peer.ip,
      nethash: opts.nethash,
      network_address: opts.network_address,
      port: peer.port,
      protocol: "http", # :(
      version: peer.version
    })
  end

  def client_from_peer(peer, %{delegate_address: "P" <> _remainer} = opts) do
    PC.new(%{
      ip: peer.ip,
      nethash: opts.nethash,
      network_address: opts.network_address,
      port: peer.port,
      protocol: "http", # :(
      version: peer.version
    })
  end

  def epoch(%{delegate_address: "A" <> _remainer}) do
    ATB.ark_epoch()
  end

  def epoch(%{delegate_address: "D" <> _remainer}) do
    ATB.ark_epoch()
  end

  def epoch(%{delegate_address: "P" <> _remainer}) do
    PTB.persona_epoch()
  end

  def fetch_network_address(%{delegate_address: "A" <> _remainer} = opts) do
    Map.put(opts, :network_address, AC.mainnet_network_address())
  end

  def fetch_network_address(%{delegate_address: "D" <> _remainer} = opts) do
    Map.put(opts, :network_address, AC.devnet_network_address())
  end

  def fetch_network_address(%{delegate_address: "P" <> _remainer} = opts) do
    Map.put(opts, :network_address, PC.mainnet_network_address())
  end

  def fetch_network_address(_) do
    raise "Invalid delegate address! Please remove config.json and restart."
  end

  def fetch_network_hash(%{delegate_address: "A" <> _remainder} = opts) do
    Map.put(opts, :nethash, @mainnet_nethash)
  end

  def fetch_network_hash(%{delegate_address: "D" <> _remainder} = opts) do
    Map.put(opts, :nethash, @devnet_nethash)
  end

  def fetch_network_hash(%{delegate_address: "P" <> _remainder} = opts) do
    Map.put(opts, :nethash, @mainnet_nethash_persona)
  end

  def fetch_network_hash(_) do
    raise "Invalid delegate address! Please remove config.json and restart."
  end

  def load_client(%{delegate_address: "P" <> _remainder} = opts) do
    client = PC.new(%{
      nethash: opts.nethash,
      network_address: opts.network_address,
      url: opts.node_url,
      version: "0.0.1"
    })

    Map.put(opts, :client, client)
  end

  def load_client(opts) do
    client = AC.new(%{
      nethash: opts.nethash,
      network_address: opts.network_address,
      url: opts.node_url,
      version: "1.1.1"
    })

    Map.put(opts, :client, client)
  end

  def load_delegate_public_key(
    %{client: client, delegate_address: delegate_address} = opts
  ) do
    case String.first(delegate_address) do
      "P" <> _remainer ->
        {:ok, public_key} = PAccount.publickey(client, delegate_address)
        Map.put(opts, :delegate_public_key, public_key)
      _ ->
        {:ok, public_key} = AAccount.publickey(client, delegate_address)
        Map.put(opts, :delegate_public_key, public_key)
    end
  end

  def timestamp(block, %{delegate_address: "A" <> _remainer}) do
    ATB.ark_epoch() + block.timestamp
  end

  def timestamp(block, %{delegate_address: "D" <> _remainer}) do
    ATB.ark_epoch() + block.timestamp
  end

  def timestamp(block, %{delegate_address: "P" <> _remainer}) do
    PTB.persona_epoch() + block.timestamp
  end

  def transaction_builder(%{delegate_address: "A" <> _remainer}) do
    ATB
  end

  def transaction_builder(%{delegate_address: "D" <> _remainer}) do
    ATB
  end

  def transaction_builder(%{delegate_address: "P" <> _remainer}) do
    PTB
  end
end
